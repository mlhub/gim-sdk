<?php

namespace Mlh\GimSdk\Tests\SignatureGenerator;

use Mlh\GimSdk\Tests\TestCase;
use Mlh\GimSdk\SignatureGenerator;

/**
 * Class SignatureGeneratorTest
 * @package Mlh\GimSdk\Tests\SignatureGenerator
 */
class SignatureGeneratorTest extends TestCase
{
    /**
     * @var SignatureGenerator
     */
    private $generator;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->generator = new SignatureGenerator;
    }

    /**
     * @return void
     */
    public function testGenerate(): void
    {
        $data = [
            'email' => $this->faker->email,
        ];

        $signature = $this->generator->generate($this->faker->word(), $data);

        $this->assertNotNull($signature);
        $this->assertNotNull(json_encode($signature));
        $this->assertEquals(JSON_ERROR_NONE, json_last_error());
    }

    /**
     * @return void
     */
    public function testTransformRequestForSignature(): void
    {
        $instanceName = $this->faker->word();
        $data = [
            'email' => $this->faker->email,
            'login' => $this->faker->word,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
        ];
        $expected = $instanceName . json_encode($data);

        $this->assertEquals($expected, $this->generator->transformRequestForSignature($instanceName, $data));
    }
}