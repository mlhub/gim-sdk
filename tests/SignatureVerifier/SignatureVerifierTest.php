<?php

namespace Mlh\GimSdk\Tests\SignatureVerifier;

use Mlh\GimSdk\Tests\TestCase;
use Mlh\GimSdk\SignatureVerifier;
use Mlh\GimSdk\SignatureGenerator;
use Mlh\GimSdk\Tests\Utils\IntegrationSettingsRepository;

/**
 * Class SignatureVerifierTest
 * @package Mlh\GimSdk\Tests\SignatureVerifier
 */
class SignatureVerifierTest extends TestCase
{
    /**
     * @var SignatureGenerator
     */
    private $generator;
    /**
     * @var IntegrationSettingsRepository
     */
    private $repository;
    /**
     * @var SignatureVerifier
     */
    private $verifier;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->generator = new SignatureGenerator;
        $this->repository = new IntegrationSettingsRepository;
        $this->verifier = new SignatureVerifier($this->generator, $this->repository);
    }

    /**
     * @return void
     */
    public function testVerifyTrueCase(): void
    {
        $data = [
            'email' => $this->faker->email,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
        ];
        $signature = $this->generator->generate($this->repository->getInstanceName(), $data);

        $this->assertTrue($this->verifier->verify($signature, $data));
    }

    /**
     * @return void
     */
    public function testVerifyFalseCase(): void
    {
        $data = [
            'email' => $this->faker->email,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
        ];

        $this->assertFalse($this->verifier->verify($this->faker->word, $data));
    }
}