<?php

namespace Mlh\GimSdk\Tests\Utils;

use Carbon\Carbon;
use Faker\Generator;
use Mlh\GimSdk\Interfaces\Course as CourseInterface;

/**
 * Class Course
 * @package Mlh\GimSdk\Tests\Utils
 */
class Course implements CourseInterface
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $title;

    /**
     * LmsUser constructor.
     * @param Generator $faker
     */
    public function __construct(Generator $faker)
    {
        $this->id = $faker->randomNumber(3, true);
        $this->title = $faker->title;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return Carbon::now();
    }
}