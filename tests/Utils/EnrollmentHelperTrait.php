<?php


namespace Mlh\GimSdk\Tests\Utils;

use Mlh\GimSdk\Interfaces\IntegrationSettingsRepository as IntegrationSettingsRepositoryInterface;
use Mlh\GimSdk\Interfaces\Enrollment as EnrollmentInterface;

/**
 * Trait EnrollmentHelperTrait
 * @package Mlh\GimSdk\Tests\Utils
 */
trait EnrollmentHelperTrait
{
    /**
     * @param IntegrationSettingsRepositoryInterface $repository
     * @param EnrollmentInterface $enrollment
     * @return array
     */
    public function getMultipartRequestData(IntegrationSettingsRepositoryInterface $repository, EnrollmentInterface $enrollment): array
    {
        return [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $repository->getAccessToken(),
            ],
            'multipart' => [
                [
                    'name' => 'instance_name',
                    'contents' => $repository->getInstanceName(),
                ],
                [
                    'name' => 'external_id',
                    'contents' => $enrollment->getId(),
                ],
                [
                    'name' => 'lms_user_external_id',
                    'contents' => $enrollment->getUserId(),
                ],
                [
                    'name' => 'course_external_id',
                    'contents' => $enrollment->getCourseId(),
                ],
                [
                    'name' => 'status',
                    'contents' => $enrollment->getStatus(),
                ],
                [
                    'name' => 'archived',
                    'contents' => $enrollment->isArchived(),
                ],
                [
                    'name' => 'compliant_date',
                    'contents' => $enrollment->getCompliantDate(),
                ],
                [
                    'name' => 'expiry_date',
                    'contents' => $enrollment->getExpiryDate(),
                ],
                [
                    'name' => 'created_at',
                    'contents' => $enrollment->getCreatedAt(),
                ],
                [
                    'name' => 'certificate',
                    'contents' => $enrollment->getCertificatePath(),
                ],
            ]
        ];
    }
}