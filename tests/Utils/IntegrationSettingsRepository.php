<?php

namespace Mlh\GimSdk\Tests\Utils;

use Carbon\Carbon;
use Mlh\GimSdk\Interfaces\IntegrationSettingsRepository as IntegrationSettingsRepositoryInterface;

/**
 * Class IntegrationSettingsRepository
 * @package Mlh\GimSdk\Tests\Utils
 */
class IntegrationSettingsRepository implements IntegrationSettingsRepositoryInterface
{
    /**
     * @var string|null
     */
    private $token;
    /**
     * @var Carbon|null
     */
    private $expiresAt;
    /**
     * @var boolean
     */
    private $integrationEnabled;

    /**
     * IntegrationSettingsRepository constructor.
     * @param string|null $token
     * @param Carbon|null $expiresAt
     */
    public function __construct(?string $token = null, ?Carbon $expiresAt = null)
    {
        $this->token = $token;
        $this->expiresAt = $expiresAt;
        $this->integrationEnabled = true;
    }

    /**
     * @return string|null
     */
    public function getAccessToken(): ?string
    {
        return $this->token;
    }

    /**
     * @return Carbon|null
     */
    public function getAccessTokenExpiresAt(): ?Carbon
    {
        return $this->expiresAt;
    }

    /**
     * @param string $token
     * @param Carbon $expiresAt
     * @return void
     */
    public function saveAccessToken(string $token, Carbon $expiresAt): void
    {
        $this->token = $token;
        $this->expiresAt = $expiresAt;
    }

    /**
     * @return string|null
     */
    public function getInstanceName(): ?string
    {
        return 'instance_name';
    }

    /**
     * @return string
     */
    public function getApiUrl(): string
    {
        return 'url';
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return 'client_id';
    }

    /**
     * @return string
     */
    public function getClientSecret(): string
    {
        return 'client_secret';
    }

    /**
     * @return bool
     */
    public function isIntegrationEnabled(): bool
    {
        return $this->integrationEnabled;
    }

    /**
     * @param bool $value
     */
    public function setIntegrationEnabled(bool $value): void
    {
        $this->integrationEnabled = $value;
    }
}
