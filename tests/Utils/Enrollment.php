<?php

namespace Mlh\GimSdk\Tests\Utils;

use Carbon\Carbon;
use Faker\Generator;
use Mlh\GimSdk\Interfaces\Enrollment as EnrollmentInterface;

/**
 * Class Enrollment
 * @package Mlh\GimSdk\Tests\Utils
 */
class Enrollment implements EnrollmentInterface
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $userId;
    /**
     * @var int
     */
    private $courseId;
    /**
     * @var int
     */
    private $status;
    /**
     * @var bool
     */
    private $archived;
    /**
     * @var Carbon
     */
    private $compliantDate;
    /**
     * @var Carbon
     */
    private $expiryDate;


    /**
     * LmsUser constructor.
     * @param Generator $faker
     */
    public function __construct(Generator $faker)
    {
        $this->id = $faker->randomNumber(3, true);
        $this->userId = $faker->randomNumber(3, true);
        $this->courseId = $faker->randomNumber(3, true);
        $this->status = $faker->randomElement(self::STATUSES);
        $this->archived = $faker->boolean();
        $this->compliantDate = Carbon::now();
        $this->expiryDate = Carbon::now();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getCourseId(): int
    {
        return $this->courseId;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function isArchived(): int
    {
        return $this->archived;
    }

    /**
     * @return Carbon
     */
    public function getCompliantDate(): Carbon
    {
        return $this->compliantDate;
    }

    /**
     * @return Carbon
     */
    public function getExpiryDate(): Carbon
    {
        return $this->expiryDate;
    }

    /**
     * @return string|null
     */
    public function getCertificatePath(): ?string
    {
        return null;
    }

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return Carbon::now();
    }
}