<?php

namespace Mlh\GimSdk\Tests\Utils;

use Carbon\Carbon;
use Faker\Generator;
use Mlh\GimSdk\Interfaces\LmsUser as LmsUserInterface;

/**
 * Class LmsUser
 * @package Mlh\GimSdk\Tests\Utils
 */
class LmsUser implements LmsUserInterface
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $login;
    /**
     * @var string
     */
    private $firstName;
    /**
     * @var string
     */
    private $lastName;
    /**
     * @var string
     */
    private $jobTitle;
    /**
     * @var string
     */
    private $department;
    /**
     * @var string
     */
    private $division;

    /**
     * LmsUser constructor.
     * @param Generator $faker
     */
    public function __construct(Generator $faker)
    {
        $this->id = $faker->randomNumber(3, true);
        $this->email = $faker->email;
        $this->login = $faker->word;
        $this->firstName = $faker->firstName;
        $this->lastName = $faker->lastName;
        $this->jobTitle = $faker->jobTitle;
        $this->department = $faker->jobTitle;
        $this->division = $faker->jobTitle;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getJobTitle(): ?string
    {
        return $this->jobTitle;
    }

    public function getDepartment(): ?string
    {
        return $this->department;
    }

    public function getDivision(): ?string
    {
        return $this->division;
    }

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return Carbon::now();
    }
}
