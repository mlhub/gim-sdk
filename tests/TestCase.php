<?php

namespace Mlh\GimSdk\Tests;

use Mockery;
use Carbon\Carbon;
use Faker\Factory;
use Faker\Generator;
use GuzzleHttp\Client;
use GuzzleHttp\Middleware;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Handler\MockHandler;
use Mlh\GimSdk\Tests\Utils\IntegrationSettingsRepository;

/**
 * Class TestCase
 * @package Mlh\GimSdk\Tests
 */
class TestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Generator
     */
    protected $faker;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->faker = Factory::create();
        Carbon::setTestNow(Carbon::now());
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        if (class_exists('Mockery')) {
            if ($container = Mockery::getContainer()) {
                $this->addToAssertionCount($container->mockery_getExpectationCount());
            }

            Mockery::close();
        }
    }

    /**
     * @param array $mockResponses
     * @param array $requestHistory
     * @return Client
     */
    protected function createGuzzleClient(array $mockResponses, array &$requestHistory): Client
    {
        $mock = new MockHandler($mockResponses);
        $handlerStack = HandlerStack::create($mock);
        $handlerStack->push(Middleware::history($requestHistory));

        return new Client(['handler' => $handlerStack]);
    }

    /**
     * @return IntegrationSettingsRepository
     */
    protected function createIntegrationSettingsRepository(): IntegrationSettingsRepository
    {
        return new IntegrationSettingsRepository(
            $this->faker->sha256,
            Carbon::now()->addWeek()
        );
    }

    /**
     * @param Request $request
     * @param string $expectedUri
     * @return void
     */
    protected function checkRequestUri(Request $request, string $expectedUri): void
    {
        $this->assertEquals($expectedUri, $request->getUri()->__toString());
    }

    /**
     * @param Request $request
     * @param array $expectedBody
     * @return void
     */
    protected function checkRequestBody(Request $request, array $expectedBody): void
    {
        $this->assertEquals($expectedBody, json_decode($request->getBody()->getContents(), true));
    }
}
