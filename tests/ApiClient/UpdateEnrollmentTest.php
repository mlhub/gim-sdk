<?php

namespace Mlh\GimSdk\Tests\ApiClient;

use Mockery;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Mlh\GimSdk\ApiClient;
use GuzzleHttp\Psr7\Response;
use Mlh\GimSdk\Tests\TestCase;
use Mlh\GimSdk\Tests\Utils\Enrollment;
use GuzzleHttp\Exception\GuzzleException;
use Mlh\GimSdk\Exceptions\WebhookException;
use Mlh\GimSdk\Exceptions\RefreshTokenException;
use Mlh\GimSdk\Tests\Utils\EnrollmentHelperTrait;
use Mlh\GimSdk\Tests\Utils\IntegrationSettingsRepository;

/**
 * Class UpdateEnrollmentTest
 * @package Mlh\GimSdk\Tests\ApiClient
 */
class UpdateEnrollmentTest extends TestCase
{
    use EnrollmentHelperTrait;

    /**
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function testIntegrationIsNotEnabled(): void
    {
        $requestHistory = [];
        $client = $this->createGuzzleClient([
            new Response(200),
        ], $requestHistory);
        $repository = $this->createIntegrationSettingsRepository();
        $repository->setIntegrationEnabled(false);
        $apiClient = new ApiClient($client, $repository);

        $apiClient->updateEnrollment(new Enrollment($this->faker));

        $this->assertEmpty($requestHistory);
    }

    /**
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function testWebhookException(): void
    {
        $requestHistory = [];
        $client = $this->createGuzzleClient([
            new Response(404),
        ], $requestHistory);
        $apiClient = new ApiClient($client, $this->createIntegrationSettingsRepository());

        $this->expectException(WebhookException::class);
        $apiClient->updateEnrollment(new Enrollment($this->faker));
    }

    /**
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function testRefreshTokenException(): void
    {
        $requestHistory = [];
        $client = $this->createGuzzleClient([
            new Response(401),
            new Response(404),
        ], $requestHistory);
        $apiClient = new ApiClient($client, $this->createIntegrationSettingsRepository());

        $this->expectException(RefreshTokenException::class);
        $apiClient->updateEnrollment(new Enrollment($this->faker));
    }

    /**
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function testUnauthorized(): void
    {
        $requestHistory = [];
        $accessToken = $this->faker->sha256;
        $expiresIn = $this->faker->randomNumber(5, true);
        $client = $this->createGuzzleClient([
            new Response(401),
            new Response(200, [], json_encode(['access_token' => $accessToken, 'expires_in' => $expiresIn])),
            new Response(200),
        ], $requestHistory);
        $repository = $this->createIntegrationSettingsRepository();
        $apiClient = new ApiClient($client, $repository);
        $enrollment = new Enrollment($this->faker);

        $apiClient->updateEnrollment($enrollment);

        $this->assertEquals($accessToken, $repository->getAccessToken());
        $this->assertEquals(Carbon::now()->addSeconds($expiresIn), $repository->getAccessTokenExpiresAt());
        $this->checkRequestUri(
            $requestHistory[0]['request'],
            $repository->getApiUrl() . ApiClient::UPDATE_ENROLLMENT_URL
        );
        $this->checkRequestUri(
            $requestHistory[1]['request'],
            $repository->getApiUrl() . ApiClient::OAUTH_TOKEN_URL
        );
        $this->checkRequestUri(
            $requestHistory[2]['request'],
            $repository->getApiUrl() . ApiClient::UPDATE_ENROLLMENT_URL
        );
    }

    /**
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function testTokenDoesNotExists(): void
    {
        $requestHistory = [];
        $accessToken = $this->faker->sha256;
        $expiresIn = $this->faker->randomNumber(5, true);
        $client = $this->createGuzzleClient([
            new Response(200, [], json_encode(['access_token' => $accessToken, 'expires_in' => $expiresIn])),
            new Response(200),
        ], $requestHistory);
        $repository = new IntegrationSettingsRepository;
        $apiClient = new ApiClient($client, $repository);
        $enrollment = new Enrollment($this->faker);

        $apiClient->updateEnrollment($enrollment);

        $this->assertEquals($accessToken, $repository->getAccessToken());
        $this->assertEquals(Carbon::now()->addSeconds($expiresIn), $repository->getAccessTokenExpiresAt());
        $this->checkRequestUri(
            $requestHistory[0]['request'],
            $repository->getApiUrl() . ApiClient::OAUTH_TOKEN_URL
        );
        $this->checkRequestUri(
            $requestHistory[1]['request'],
            $repository->getApiUrl() . ApiClient::UPDATE_ENROLLMENT_URL
        );
    }

    /**
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function testOk(): void
    {
        $enrollment = new Enrollment($this->faker);
        $repository = $this->createIntegrationSettingsRepository();

        $client = Mockery::mock(Client::class);
        $client->shouldReceive('post')
            ->with($repository->getApiUrl() . ApiClient::UPDATE_ENROLLMENT_URL, $this->getMultipartRequestData($repository, $enrollment))
            ->once();

        $apiClient = new ApiClient($client, $repository);

        $apiClient->updateEnrollment($enrollment);
    }
}