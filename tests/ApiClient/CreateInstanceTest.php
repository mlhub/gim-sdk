<?php

namespace Mlh\GimSdk\Tests\ApiClient;

use Carbon\Carbon;
use Mlh\GimSdk\ApiClient;
use GuzzleHttp\Psr7\Response;
use Mlh\GimSdk\Tests\TestCase;
use GuzzleHttp\Exception\GuzzleException;
use Mlh\GimSdk\Exceptions\WebhookException;
use Mlh\GimSdk\Exceptions\RefreshTokenException;
use Mlh\GimSdk\Tests\Utils\IntegrationSettingsRepository;

/**
 * Class CreateInstanceTest
 * @package Mlh\GimSdk\Tests\ApiClient
 */
class CreateInstanceTest extends TestCase
{
    /**
     * @return void
     * @throws RefreshTokenException
     * @throws WebhookException
     * @throws GuzzleException
     */
    public function testIntegrationIsNotEnabled(): void
    {
        $requestHistory = [];
        $client = $this->createGuzzleClient([
            new Response(200),
        ], $requestHistory);
        $repository = $this->createIntegrationSettingsRepository();
        $repository->setIntegrationEnabled(false);
        $apiClient = new ApiClient($client, $repository);

        $apiClient->createInstance($this->faker->word);

        $this->assertEmpty($requestHistory);
    }

    /**
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function testWebhookException(): void
    {
        $requestHistory = [];
        $client = $this->createGuzzleClient([
            new Response(404),
        ], $requestHistory);
        $apiClient = new ApiClient($client, $this->createIntegrationSettingsRepository());

        $this->expectException(WebhookException::class);
        $apiClient->createInstance($this->faker->word);
    }

    /**
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function testRefreshTokenException(): void
    {
        $requestHistory = [];
        $client = $this->createGuzzleClient([
            new Response(401),
            new Response(404),
        ], $requestHistory);
        $apiClient = new ApiClient($client, $this->createIntegrationSettingsRepository());

        $this->expectException(RefreshTokenException::class);
        $apiClient->createInstance($this->faker->word);
    }

    /**
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function testUnauthorized(): void
    {
        $requestHistory = [];
        $accessToken = $this->faker->sha256;
        $expiresIn = $this->faker->randomNumber(5, true);
        $client = $this->createGuzzleClient([
            new Response(401),
            new Response(200, [], json_encode(['access_token' => $accessToken, 'expires_in' => $expiresIn])),
            new Response(200),
        ], $requestHistory);
        $repository = $this->createIntegrationSettingsRepository();
        $apiClient = new ApiClient($client, $repository);
        $instanceName = $this->faker->word;

        $apiClient->createInstance($instanceName);

        $this->assertEquals($accessToken, $repository->getAccessToken());
        $this->assertEquals(Carbon::now()->addSeconds($expiresIn), $repository->getAccessTokenExpiresAt());
        $this->checkRequestUri(
            $requestHistory[0]['request'],
            $repository->getApiUrl() . ApiClient::CREATE_INSTANCE_URL
        );
        $this->checkRequestBody(
            $requestHistory[0]['request'],
            [
                'name' => $instanceName,
            ]
        );
        $this->checkRequestUri(
            $requestHistory[1]['request'],
            $repository->getApiUrl() . ApiClient::OAUTH_TOKEN_URL
        );
        $this->checkRequestBody(
            $requestHistory[1]['request'],
            [
                'grant_type' => 'client_credentials',
                'client_id' => $repository->getClientId(),
                'client_secret' => $repository->getClientSecret(),
            ]
        );
        $this->checkRequestUri(
            $requestHistory[2]['request'],
            $repository->getApiUrl() . ApiClient::CREATE_INSTANCE_URL
        );
        $this->checkRequestBody(
            $requestHistory[2]['request'],
            [
                'name' => $instanceName,
            ]
        );
    }

    /**
     * @return void
     * @throws RefreshTokenException
     * @throws WebhookException
     * @throws GuzzleException
     */
    public function testTokenDoesNotExists(): void
    {
        $requestHistory = [];
        $accessToken = $this->faker->sha256;
        $expiresIn = $this->faker->randomNumber(5, true);
        $client = $this->createGuzzleClient([
            new Response(200, [], json_encode(['access_token' => $accessToken, 'expires_in' => $expiresIn])),
            new Response(200),
        ], $requestHistory);
        $repository = new IntegrationSettingsRepository;
        $apiClient = new ApiClient($client, $repository);
        $instanceName = $this->faker->word;

        $apiClient->createInstance($instanceName);

        $this->assertEquals($accessToken, $repository->getAccessToken());
        $this->assertEquals(Carbon::now()->addSeconds($expiresIn), $repository->getAccessTokenExpiresAt());
        $this->checkRequestUri(
            $requestHistory[0]['request'],
            $repository->getApiUrl() . ApiClient::OAUTH_TOKEN_URL
        );
        $this->checkRequestBody(
            $requestHistory[0]['request'],
            [
                'grant_type' => 'client_credentials',
                'client_id' => $repository->getClientId(),
                'client_secret' => $repository->getClientSecret(),
            ]
        );
        $this->checkRequestUri(
            $requestHistory[1]['request'],
            $repository->getApiUrl() . ApiClient::CREATE_INSTANCE_URL
        );
        $this->checkRequestBody(
            $requestHistory[1]['request'],
            [
                'name' => $instanceName,
            ]
        );
    }

    /**
     * @return void
     * @throws RefreshTokenException
     * @throws WebhookException
     * @throws GuzzleException
     */
    public function testOk(): void
    {
        $requestHistory = [];
        $client = $this->createGuzzleClient([
            new Response(200),
        ], $requestHistory);
        $repository = $this->createIntegrationSettingsRepository();
        $apiClient = new ApiClient($client, $repository);
        $instanceName = $this->faker->word;

        $apiClient->createInstance($instanceName);

        $this->checkRequestUri(
            $requestHistory[0]['request'],
            $repository->getApiUrl() . ApiClient::CREATE_INSTANCE_URL
        );
        $this->checkRequestBody(
            $requestHistory[0]['request'],
            [
                'name' => $instanceName,
            ]
        );
    }
}
