<?php

return [
    'enabled' => env('GIM_ENABLED', false),
    'url' => env('GIM_URL'),
    'client_id' => env('GIM_CLIENT_ID'),
    'client_secret' => env('GIM_CLIENT_SECRET'),
];
