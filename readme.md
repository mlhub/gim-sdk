## Installation

### Add repository to composer.json

`"repositories": [{"type": "git","url": "https://bitbucket.org/mlhub/gim-sdk.git"}]`

### Install package with composer

`composer require mlh/gim-sdk`

### Implement interface IntegrationSettingsRepository and register your implementation

`$this->app->bind(IntegrationSettingsRepositoryInterface::class, IntegrationSettingsRepository::class);`

## Basic Usage

#### Make it and use it :)

`$this->apiClient = app()->make(ApiClient::class);`<br>
`$this->apiClient->createInstance($instanceName);`<br>
`$this->apiClient->deleteInstance($instanceName);`<br>
`$this->apiClient->createLmsUser($user);`<br>
`$this->apiClient->updateLmsUser($user);`<br>
`$this->apiClient->deleteLmsUser($user);`<br>