<?php

namespace Mlh\GimSdk;

use Mlh\GimSdk\Interfaces\IntegrationSettingsRepository;
use Mlh\GimSdk\Interfaces\SignatureVerifier as SignatureVerifierInterface;
use RuntimeException;

/**
 * Class SignatureVerifier
 * @package Mlh\GimSdk
 */
class SignatureVerifier implements SignatureVerifierInterface
{
    /**
     * @var SignatureGenerator
     */
    private $generator;
    /**
     * @var IntegrationSettingsRepository
     */
    private $repository;

    /**
     * SignatureVerifier constructor.
     */
    public function __construct(SignatureGenerator $generator, IntegrationSettingsRepository $repository)
    {
        $this->generator = $generator;
        $this->repository = $repository;
    }

    /**
     * @param string $signature
     * @param array $requestData
     * @return bool
     */
    public function verify(string $signature, array $requestData): bool
    {
        $dataToVerify = $this->generator->transformRequestForSignature($this->repository->getInstanceName(), $requestData);
        $result = openssl_verify($dataToVerify, base64_decode($signature), $this->getKey(), OPENSSL_ALGO_SHA256);

        if ($result === 1) {
            return true;
        }

        if ($result === 0) {
            return false;
        }

        throw new RuntimeException('An error occurred while verifying the signature.');
    }

    /**
     * @return false|resource
     */
    private function getKey()
    {
        $fp = fopen(__DIR__ . '/../keys/public.key', 'rb');
        $key = fread($fp, 8192);
        fclose($fp);

        return openssl_pkey_get_public($key);
    }
}