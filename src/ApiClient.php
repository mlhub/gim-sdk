<?php

namespace Mlh\GimSdk;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Utils;
use Mlh\GimSdk\Interfaces\Course;
use Mlh\GimSdk\Interfaces\LmsUser;
use Mlh\GimSdk\Interfaces\Enrollment;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Mlh\GimSdk\Exceptions\WebhookException;
use Mlh\GimSdk\Exceptions\RefreshTokenException;
use Mlh\GimSdk\Interfaces\IntegrationSettingsRepository;
use Mlh\GimSdk\Interfaces\ApiClient as ApiClientInterface;

/**
 * Class ApiClient
 * @package Mlh\GimSdk
 */
class ApiClient implements ApiClientInterface
{
    public const OAUTH_TOKEN_URL = '/oauth/token';

    public const CREATE_INSTANCE_URL = '/api/v1/instances/create';
    public const DELETE_INSTANCE_URL = '/api/v1/instances/delete';

    public const CREATE_LMS_USER_URL = '/api/v1/lms_users/create';
    public const UPDATE_LMS_USER_URL = '/api/v1/lms_users/update';
    public const ARCHIVE_LMS_USER_URL = '/api/v1/lms_users/archive';
    public const RESTORE_LMS_USER_URL = '/api/v1/lms_users/restore';
    public const DELETE_LMS_USER_URL = '/api/v1/lms_users/delete';

    public const CREATE_COURSE_URL = '/api/v1/courses/create';
    public const UPDATE_COURSE_URL = '/api/v1/courses/update';

    public const CREATE_ENROLLMENT_URL = '/api/v1/enrollments/create';
    public const UPDATE_ENROLLMENT_URL = '/api/v1/enrollments/update';
    public const DELETE_ENROLLMENT_URL = '/api/v1/enrollments/delete';

    /**
     * @var Client
     */
    private $client;
    /**
     * @var IntegrationSettingsRepository
     */
    private $repository;

    /**
     * ApiClient constructor.
     * @param Client $client
     * @param IntegrationSettingsRepository $repository
     */
    public function __construct(Client $client, IntegrationSettingsRepository $repository)
    {
        $this->client = $client;
        $this->repository = $repository;
    }

    /**
     * @param string $name
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function createInstance(string $name): void
    {
        $this->sendPostRequest(self::CREATE_INSTANCE_URL, [
            'json' => [
                'name' => $name,
            ],
        ]);
    }

    /**
     * @param string $name
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function deleteInstance(string $name): void
    {
        $this->sendPostRequest(self::DELETE_INSTANCE_URL, [
            'json' => [
                'name' => $name,
            ],
        ]);
    }

    /**
     * @param LmsUser $lmsUser
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function createLmsUser(LmsUser $lmsUser): void
    {
        $this->sendPostRequest(self::CREATE_LMS_USER_URL, [
            'json' => [
                'instance_name' => $this->repository->getInstanceName(),
                'external_id' => $lmsUser->getId(),
                'email' => $lmsUser->getEmail(),
                'login' => $lmsUser->getLogin(),
                'first_name' => $lmsUser->getFirstName(),
                'last_name' => $lmsUser->getLastName(),
                'job_title' => $lmsUser->getJobTitle(),
                'department' => $lmsUser->getDepartment(),
                'division' => $lmsUser->getDivision(),
                'created_at' => $lmsUser->getCreatedAt(),
            ],
        ]);
    }

    /**
     * @param LmsUser $lmsUser
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function updateLmsUser(LmsUser $lmsUser): void
    {
        $this->sendPostRequest(self::UPDATE_LMS_USER_URL, [
            'json' => [
                'instance_name' => $this->repository->getInstanceName(),
                'external_id' => $lmsUser->getId(),
                'email' => $lmsUser->getEmail(),
                'login' => $lmsUser->getLogin(),
                'first_name' => $lmsUser->getFirstName(),
                'last_name' => $lmsUser->getLastName(),
                'job_title' => $lmsUser->getJobTitle(),
                'department' => $lmsUser->getDepartment(),
                'division' => $lmsUser->getDivision(),
            ],
        ]);
    }

    /**
     * @param LmsUser $lmsUser
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function archiveLmsUser(LmsUser $lmsUser): void
    {
        $this->sendPostRequest(self::ARCHIVE_LMS_USER_URL, [
            'json' => [
                'instance_name' => $this->repository->getInstanceName(),
                'external_id' => $lmsUser->getId(),
            ],
        ]);
    }

    /**
     * @param LmsUser $lmsUser
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function restoreLmsUser(LmsUser $lmsUser): void
    {
        $this->sendPostRequest(self::RESTORE_LMS_USER_URL, [
            'json' => [
                'instance_name' => $this->repository->getInstanceName(),
                'external_id' => $lmsUser->getId(),
            ],
        ]);
    }

    /**
     * @param LmsUser $lmsUser
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function deleteLmsUser(LmsUser $lmsUser): void
    {
        $this->sendPostRequest(self::DELETE_LMS_USER_URL, [
            'json' => [
                'instance_name' => $this->repository->getInstanceName(),
                'external_id' => $lmsUser->getId(),
            ],
        ]);
    }

    /**
     * @param Course $course
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function createCourse(Course $course): void
    {
        $this->sendPostRequest(self::CREATE_COURSE_URL, [
            'json' => [
                'instance_name' => $this->repository->getInstanceName(),
                'external_id' => $course->getId(),
                'title' => $course->getTitle(),
                'created_at' => $course->getCreatedAt(),
            ],
        ]);
    }

    /**
     * @param Course $course
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function updateCourse(Course $course): void
    {
        $this->sendPostRequest(self::UPDATE_COURSE_URL, [
            'json' => [
                'instance_name' => $this->repository->getInstanceName(),
                'external_id' => $course->getId(),
                'title' => $course->getTitle(),
            ],
        ]);
    }

    /**
     * @param Enrollment $enrollment
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function createEnrollment(Enrollment $enrollment): void
    {
        $this->sendPostRequest(self::CREATE_ENROLLMENT_URL, $this->getEnrollmentMultipartData($enrollment));
    }

    /**
     * @param Enrollment $enrollment
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function updateEnrollment(Enrollment $enrollment): void
    {
        $this->sendPostRequest(self::UPDATE_ENROLLMENT_URL, $this->getEnrollmentMultipartData($enrollment));
    }

    /**
     * @param Enrollment $enrollment
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    public function deleteEnrollment(Enrollment $enrollment): void
    {
        $this->sendPostRequest(self::DELETE_ENROLLMENT_URL, [
            'json' => [
                'instance_name' => $this->repository->getInstanceName(),
                'external_id' => $enrollment->getId(),
            ],
        ]);
    }

    /**
     * @param Enrollment $enrollment
     * @return array
     */
    private function getEnrollmentMultipartData(Enrollment $enrollment): array
    {
        return [
            'multipart' => [
                [
                    'name' => 'instance_name',
                    'contents' => $this->repository->getInstanceName(),
                ],
                [
                    'name' => 'external_id',
                    'contents' => $enrollment->getId(),
                ],
                [
                    'name' => 'lms_user_external_id',
                    'contents' => $enrollment->getUserId(),
                ],
                [
                    'name' => 'course_external_id',
                    'contents' => $enrollment->getCourseId(),
                ],
                [
                    'name' => 'status',
                    'contents' => $enrollment->getStatus(),
                ],
                [
                    'name' => 'archived',
                    'contents' => $enrollment->isArchived(),
                ],
                [
                    'name' => 'compliant_date',
                    'contents' => $enrollment->getCompliantDate(),
                ],
                [
                    'name' => 'expiry_date',
                    'contents' => $enrollment->getExpiryDate(),
                ],
                [
                    'name' => 'created_at',
                    'contents' => $enrollment->getCreatedAt(),
                ],
                [
                    'name' => 'certificate',
                    'contents' => $enrollment->getCertificatePath() ? Utils::tryFopen($enrollment->getCertificatePath(), 'r') : null,
                ],
            ]
        ];
    }

    /**
     * @param string $urn
     * @param array $data
     * @return void
     * @throws GuzzleException
     * @throws RefreshTokenException
     * @throws WebhookException
     */
    private function sendPostRequest(string $urn, array $data): void
    {
        if (!$this->repository->isIntegrationEnabled()) {
            return;
        }

        if ($this->repository->getAccessTokenExpiresAt() <= Carbon::now()) {
            $this->refreshAccessToken();
        }

        try {
            $headers = [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . $this->repository->getAccessToken(),
                ],
            ];
            $this->client->post(
                $this->repository->getApiUrl() . $urn,
                array_merge($headers, $data)
            );
        } catch (ClientException $exception) {
            if ($exception->getCode() === 401) {
                $this->refreshAccessToken();
                $this->sendPostRequest($urn, $data);
                return;
            }

            throw new WebhookException($exception);
        }
    }

    /**
     * @return void
     * @throws RefreshTokenException
     * @throws GuzzleException
     */
    private function refreshAccessToken(): void
    {
        try {
            $response = $this->client->post(
                $this->repository->getApiUrl() . self::OAUTH_TOKEN_URL,
                [
                    'headers' => [
                        'Accept' => 'application/json',
                    ],
                    'json' => [
                        'grant_type' => 'client_credentials',
                        'client_id' => $this->repository->getClientId(),
                        'client_secret' => $this->repository->getClientSecret(),
                    ],
                ]
            );
            $response = json_decode($response->getBody()->getContents(), true);

            $this->repository->saveAccessToken($response['access_token'], Carbon::now()->addSeconds($response['expires_in']));
        } catch (ClientException $exception) {
            throw new RefreshTokenException($exception);
        }
    }
}
