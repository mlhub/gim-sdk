<?php

namespace Mlh\GimSdk\Exceptions;

use Exception;
use Throwable;

/**
 * Class RefreshTokenException
 * @package Mlh\GimSdk\Exceptions
 */
class RefreshTokenException extends Exception
{
    /**
     * RefreshTokenException constructor.
     * @param Throwable $previous
     */
    public function __construct(Throwable $previous)
    {
        parent::__construct('Refresh token failed with message: ' . $previous->getMessage(), 401, $previous);
    }
}
