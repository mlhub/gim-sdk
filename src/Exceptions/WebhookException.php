<?php

namespace Mlh\GimSdk\Exceptions;

use Exception;
use Throwable;

/**
 * Class WebhookException
 * @package Mlh\GimSdk\Exceptions
 */
class WebhookException extends Exception
{
    /**
     * WebhookException constructor.
     * @param Throwable $previous
     */
    public function __construct(Throwable $previous)
    {
        parent::__construct('Sending following webhook failed: ' . $previous->getMessage(), $previous->getCode(), $previous);
    }
}
