<?php

namespace Mlh\GimSdk;

use RuntimeException;
use Mlh\GimSdk\Interfaces\SignatureGenerator as SignatureGeneratorInterface;

/**
 * Class SignatureGenerator
 * @package Mlh\GimSdk
 */
class SignatureGenerator implements SignatureGeneratorInterface
{
    /**
     * @param string $instanceName
     * @param array $requestData
     * @return string
     */
    public function generate(string $instanceName, array $requestData): string
    {
        if (!openssl_sign($this->transformRequestForSignature($instanceName, $requestData), $signature, $this->getKey(), OPENSSL_ALGO_SHA256)) {
            throw new RuntimeException(sprintf('OpenSSL data signing failed with error: %s', openssl_error_string()));
        }

        return base64_encode($signature);
    }

    /**
     * @return false|resource
     */
    private function getKey()
    {
        $fp = fopen(__DIR__ . '/../keys/private.key', 'rb');
        $privateKey = fread($fp, 8192);
        fclose($fp);

        return openssl_pkey_get_private($privateKey);
    }

    /**
     * @param string $instanceName
     * @param array $requestData
     * @return string
     */
    public function transformRequestForSignature(string $instanceName, array $requestData): string
    {
        return $instanceName . json_encode($requestData);
    }
}
