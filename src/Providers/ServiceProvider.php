<?php

namespace Mlh\GimSdk\Providers;

/**
 * Class ServiceProvider
 * @package Mlh\GimSdk\Providers
 */
class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * @return void
     */
    public function register(): void
    {
        parent::register();

        $this->publishes([
            __DIR__ . '/../../config/config.php' => config_path('gim.php'),
        ]);
        $this->mergeConfigFrom(__DIR__ . '/../../config/config.php', 'gim');
    }
}
