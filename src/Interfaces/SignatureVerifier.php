<?php

namespace Mlh\GimSdk\Interfaces;

/**
 * Interface SignatureVerifier
 * @package Mlh\GimSdk\Interfaces
 */
interface SignatureVerifier
{
    /**
     * @param string $signature
     * @param array $requestData
     * @return bool
     */
    public function verify(string $signature, array $requestData): bool;
}