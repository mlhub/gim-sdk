<?php

namespace Mlh\GimSdk\Interfaces;

use Carbon\Carbon;

/**
 * Interface Course
 * @package Mlh\GimSdk\Interfaces
 */
interface Course
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon;
}
