<?php

namespace Mlh\GimSdk\Interfaces;

/**
 * Interface SignatureGenerator
 * @package Mlh\GimSdk\Interfaces
 */
interface SignatureGenerator
{
    /**
     * @param string $instanceName
     * @param array $requestData
     * @return string
     */
    public function generate(string $instanceName, array $requestData): string;

    /**
     * @param string $instanceName
     * @param array $requestData
     * @return string
     */
    public function transformRequestForSignature(string $instanceName, array $requestData): string;
}