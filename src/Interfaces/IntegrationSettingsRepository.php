<?php

namespace Mlh\GimSdk\Interfaces;

use Carbon\Carbon;

/**
 * Interface IntegrationSettingsRepository
 * @package Mlh\GimSdk\Interfaces
 */
interface IntegrationSettingsRepository
{
    /**
     * @return string|null
     */
    public function getAccessToken(): ?string;

    /**
     * @return Carbon|null
     */
    public function getAccessTokenExpiresAt(): ?Carbon;

    /**
     * @param string $token
     * @param Carbon $expiresAt
     * @return void
     */
    public function saveAccessToken(string $token, Carbon $expiresAt): void;

    /**
     * @return string|null
     */
    public function getInstanceName(): ?string;

    /**
     * @return string
     */
    public function getApiUrl(): string;

    /**
     * @return string
     */
    public function getClientId(): string;

    /**
     * @return string
     */
    public function getClientSecret(): string;

    /**
     * @return bool
     */
    public function isIntegrationEnabled(): bool;
}
