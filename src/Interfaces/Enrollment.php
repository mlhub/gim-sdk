<?php

namespace Mlh\GimSdk\Interfaces;

use Carbon\Carbon;

/**
 * Interface Enrollment
 * @package Mlh\GimSdk\Interfaces
 */
interface Enrollment
{
    public const STATUS_IN_PROGRESS = 0;
    public const STATUS_COMPLIANT = 1;
    public const STATUS_NON_COMPLIANT = 2;

    public const STATUSES = [
        self::STATUS_IN_PROGRESS,
        self::STATUS_COMPLIANT,
        self::STATUS_NON_COMPLIANT,
    ];

    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return int
     */
    public function getUserId(): int;

    /**
     * @return int
     */
    public function getCourseId(): int;

    /**
     * @return int
     */
    public function getStatus(): int;

    /**
     * @return int
     */
    public function isArchived(): int;

    /**
     * @return Carbon|null
     */
    public function getCompliantDate(): ?Carbon;

    /**
     * @return Carbon|null
     */
    public function getExpiryDate(): ?Carbon;

    /**
     * @return string|null
     */
    public function getCertificatePath(): ?string;

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon;
}