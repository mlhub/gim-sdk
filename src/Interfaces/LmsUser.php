<?php

namespace Mlh\GimSdk\Interfaces;

use Carbon\Carbon;

/**
 * Interface LmsUser
 * @package Mlh\GimSdk\Interfaces
 */
interface LmsUser
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * @return string
     */
    public function getLogin(): string;

    /**
     * @return string
     */
    public function getFirstName(): string;

    /**
     * @return string
     */
    public function getLastName(): string;

    /**
     * @return string|null
     */
    public function getJobTitle(): ?string;

    /**
     * @return string|null
     */
    public function getDepartment(): ?string;

    /**
     * @return string|null
     */
    public function getDivision(): ?string;

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon;
}
