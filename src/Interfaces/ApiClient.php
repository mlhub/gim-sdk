<?php

namespace Mlh\GimSdk\Interfaces;

/**
 * Interface ApiClient
 * @package Mlh\GimSdk\Interfaces
 */
interface ApiClient
{
    /**
     * @param string $name
     * @return void
     */
    public function createInstance(string $name): void;

    /**
     * @param string $name
     * @return void
     */
    public function deleteInstance(string $name): void;

    /**
     * @param LmsUser $lmsUser
     * @return void
     */
    public function createLmsUser(LmsUser $lmsUser): void;

    /**
     * @param LmsUser $lmsUser
     * @return void
     */
    public function updateLmsUser(LmsUser $lmsUser): void;

    /**
     * @param LmsUser $lmsUser
     * @return void
     */
    public function archiveLmsUser(LmsUser $lmsUser): void;

    /**
     * @param LmsUser $lmsUser
     * @return void
     */
    public function restoreLmsUser(LmsUser $lmsUser): void;

    /**
     * @param LmsUser $lmsUser
     * @return void
     */
    public function deleteLmsUser(LmsUser $lmsUser): void;
}
